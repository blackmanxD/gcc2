from django.test import TestCase
# from .models import Usuario

# from django.test import TestCase
from django.contrib.auth.models import User
# Create your tests here.

class LogInTest(TestCase):
    """
    Prueba unitaria que prueba el inicio de sesion de un usuario prueba 4 casos posilbes\n
    * Satisfactorio
    * Fallo de alias
    * Fallo de clave
    * Fallo de alias y clave
    """
    def setUp(self):
        self.credentials_succes = {
            'username': 'test',
            'password': 'passwordtest'}
        User.objects.create_user(**self.credentials_succes)
        self.credentials_fail_username = {
            'username': 'test_fail',
            'password': 'passwordtest'}

        self.credentials_fail_password = {
            'username': 'test',
            'password': 'passwordtest_fail'}

        self.credentials_fail_both = {
            'username': 'test_fail',
            'password': 'passwordtest_fail'}

    def test_login_succes(self):
        # send login data
        response = self.client.post('/accounts/login/', self.credentials_succes, follow=True)
        # should be logged in now
        self.assertTrue(response.context['user'].is_authenticated)

    def test_login_fail_username(self):
        # send login data
        response = self.client.post('/accounts/login/', self.credentials_fail_username, follow=True)
        # should be logged in now
        self.assertFalse(response.context['user'].is_authenticated)

    def test_login_fail_password(self):
        # send login data
        response = self.client.post('/accounts/login/', self.credentials_fail_password, follow=True)
        # should be logged in now
        self.assertFalse(response.context['user'].is_authenticated)

    def test_login_fail_both(self):
        # send login data
        response = self.client.post('/accounts/login/', self.credentials_fail_both, follow=True)
        # should be logged in now
        self.assertFalse(response.context['user'].is_authenticated)
