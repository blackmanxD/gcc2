Django==2.1
django-crispy-forms==1.7.2
django-heroku==0.3.1
gunicorn==19.9.0
psycopg2==2.8.2
psycopg2-binary==2.8.2
